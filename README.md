# gpt4u-mssql-agent

## architecture

![gpt4u-mssql-agent](gpt4u-mssql-agent.png)


## queries

```
# request
❯❯ curl -XPOST "http://18.223.100.80:7634/api/question" \
--header "Content-Type: application/json" \
--data '
{
  "question": "What is the Rear Axle Used in NNR 45-150 SWB?"
}
'

# response
{"answer":"The rear axle used in the NNR 45-150 SWB is the Isuzu R050."}
```

```
# request
❯❯ curl -XPOST "http://18.223.100.80:7634/api/question" \
--header "Content-Type: application/json" \
--data '
{
  "question": "What is the Rear Axle Used in NNR 45-150 SWB?"
}
'

# response
{"answer":"The rear axle used in the NNR 45-150 SWB is the Isuzu R050."}
```

```
# request
❯❯ curl -XPOST "http://18.223.100.80:7634/api/question" \
--header "Content-Type: application/json" \
--data '
{
  "question": "What is the Displacement CC of 4JJ1-TCS engine?"
}
'

# response
{"answer":"The displacement CC of the 4JJ1-TCS engine is 2999.00."}
```


## screen-record
[gpt4u-mssql-agent.mov](https://drive.google.com/file/d/1sRVpUPretVrK70x-c7VxRs5JBxZuWGz5/view?usp=drive_link)
